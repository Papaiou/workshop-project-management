# Lexer documentation

## Module Description

A lexer is a computer program that performs a lexical analysis, dividing a string of characters into a sequence of lexical entities.

For the Pushswap project, we need to check the number of arguments given to the program.

## Prototype

```python
def lexer(arguments):
```

Where `arguments` is the array of arguments sent to the main function of the program.

## Output

This function either return the argument list if it's not empty, or `None` if it's empty.