# Output handler documentation

## Module Description

The output handler is the part of the project that write our program result on the standard output.

## Prototype

```python
def output_writer(move):
```

Where `move` is the move to report on the standard output.

## Output

The output of these functions are visible on the standard output.