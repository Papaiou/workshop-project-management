# Parser documentation

## Module Description

A parser is a computer program that browses the contents of a text or file, analysing it to check its syntax or extract elements.

For the Pushswap project, we need to check if the arguments given to the program are correctly formatted.

## Prototype

```python
def parser(argument_list):
```

Where `argument_list` is the array of arguments sent by the [lexer](lexer_documentation.md) of the program.

## Output

This function either return the argument list number array if there is no non-numerical characters, or `None` if it's there is some non-numerical characters.