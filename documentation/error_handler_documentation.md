# Error handler documentation

## Module Description

The error handler is the part of the project that handle different output when errors are declared.

## Prototypes

```python
def lexer_error():

def parser_error():
```

No arguments are needed for these functions.

## Output

The output of these functions are visible on the standard error output.