# Algorithm documentation

## Module Description

Our algorithm is based on the fact that we have 2 lists (respectively `a_list` and `b_list`) to classify our numbers.
The goal, here, is to classify numbers in ascending order in the A list and classify number in descending order in the B list. Each number swap 

## Monitor function

The monitor function is the main part of the algorithm. It is supervising the A list algorithm part and the B list algorithm part until whole A list is sorted.

### Prototype

```python
def monitor(numbers):
```

Where `numbers` is the number list sent by the [parser](parser_documentation.md).

### Output

No output is given by this function. When this function is over, algorithm part is done !

## A_list_handler && B_list_handler functions

### Prototypes

```python
def a_list_handler(a_list, b_list):

def b_list_handler(a_list, b_list):
```

Where `a_list` and `b_list` are our 2 number lists. When `a_list_handler()` begins, `b_list` argument is empty and vice-versa.

### Output

`a_list_handler` returns both `a_list` and `b_list`but `a_list` is empty at the end.
Same thing for the `b_list_handler` with `b_list` empty at the end.


## List_swap function

### Prototype

```python
    def list_swap(number_list):
```

Where `number_list` is either `a_list` or `b_list`.

### Output

This function returns the number list given as parameter with its 2 first item swapped.

## Is_a_list_sorted function

### Prototype

```python
def is_a_list_sorted(a_list):
```

Where `a_list` is `a_list`.

### Output

This function returns `True` is `a_list` is sorted. It is the exit condition of the algorithm.