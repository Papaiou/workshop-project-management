# Project management workshop

Hello everybody! Here is the project management workshop done with Pushswap project.

This documentation will quickly explain you how to deal with this repository with a "getting started". Then, you could be redirected into the other technical part of the documentation.

## Getting started.

### Requirements

First of all, you have to check your Python version : 3.10 usage only !

### Usage

Then we can start our program giving every number we want to sort in parameter to `__main__.py` :

```bash
python3 __main__.py 1 4 5 193 -24824 84 84 71
```

Result will be written on the standard output.

In cas of error, a message will be written on the standard error output.

### Testing

We can test our program with unit and integration tests. Every test are in the `/tests` directory and MUST BE launch from the root directory :

```bash
python3 -m unittest -v tests/<filename>_tests.py
```

Do not forget to choose the correct filename and replace it in the command line.

### Covering

You can also see the test covering thanks to `coverage` Python module. You can run it like that :

```bash
coverage run -m unittest tests/<filename>.py ; coverage report -m
```

A complete coverage test can be run by replacing `<filename>` by `*`.

## Technical documentation

You can find here every technical documentation parts of the project :

 - [Lexer](documentation/lexer_documentation.md)
 - [Parser](documentation/parser_documentation.md)
 - [Algorithm](documentation/algorithm_documentation.md)
 - [Output handler](documentation/output_handler_documentation.md)
 - [Error handler](documentation/error_handler_documentation.md)