import unittest

from src.lexer import lexer


class LexerTest(unittest.TestCase):
    def test_lexer1(self):
        self.assertEqual(lexer("3291"), "3291")

    def test_lexer2(self):
        self.assertEqual(lexer([4526, 268, 1213, 27163, 2644]), [4526, 268, 1213, 27163, 2644])

    def test_lexer3(self):
        self.assertEqual(lexer(""), None)

    def test_lexer4(self):
        self.assertEqual(lexer([]), None)


if __name__ == '__main__':
    unittest.main()
