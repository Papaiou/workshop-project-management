import contextlib
import io
import unittest

from src import output_handler
from src.output_handler import output_writer


class OutputTest(unittest.TestCase):
    def test_output1(self):
        with io.StringIO() as output_buffer:
            with contextlib.redirect_stdout(output_buffer):
                output_handler.is_first_move = True
                output_writer("pa")
                output_writer("pb")
                output_writer("sa")
                output_writer("sb")
                output_writer("ra")
                output_writer("rb")
                self.assertEqual(output_buffer.getvalue(), "pa, pb, sa, sb, ra, rb")


if __name__ == '__main__':
    unittest.main()
