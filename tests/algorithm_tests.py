import unittest

from src.algorithm import list_swap, is_a_list_sorted


class AlgorithmTest(unittest.TestCase):
    def test_list_swap1(self):
        self.assertEqual(list_swap([39274, -2648]), [-2648, 39274])

    def test_list_swap2(self):
        self.assertEqual(list_swap([2648, 39274]), [39274, 2648])

    def test_list_swap3(self):
        self.assertEqual(list_swap([39274, 2648, 4386, 3794, 3974, 32486]), [2648, 39274, 4386, 3794, 3974, 32486])

    def test_is_a_list_sorted1(self):
        self.assertEqual(is_a_list_sorted([39274, 2648, 4386, 3794, 3974, 32486]), False)

    def test_is_a_list_sorted2(self):
        self.assertEqual(is_a_list_sorted([39274, 39274, 39274, 39274, 39274, 39274]), True)

    def test_is_a_list_sorted3(self):
        self.assertEqual(is_a_list_sorted([39274]), True)

    def test_is_a_list_sorted4(self):
        self.assertEqual(is_a_list_sorted([2648, 4386, 5794, 9974, 32486, 32486, 40583]), True)

    def test_is_a_list_sorted5(self):
        self.assertEqual(is_a_list_sorted([32486, 583]), False)


if __name__ == '__main__':
    unittest.main()
