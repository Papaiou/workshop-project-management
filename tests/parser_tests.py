import unittest

from src.parser import parser


class ParserTest(unittest.TestCase):
    def test_parser1(self):
        self.assertEqual(parser(["39274"]), [39274])

    def test_parser2(self):
        self.assertEqual(parser(["4526", "268", "1213", "27163", "2644"]), [4526, 268, 1213, 27163, 2644])

    def test_parser3(self):
        self.assertEqual(parser(["a2836", "23872", "27284"]), None)

    def test_parser4(self):
        self.assertEqual(parser(["284642", "28624", "2746a"]), None)

    def test_parser5(self):
        self.assertEqual(parser(["a"]), None)


if __name__ == '__main__':
    unittest.main()
