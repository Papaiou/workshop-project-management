import contextlib
import io
import unittest

from src.error_handler import lexer_error, parser_error


class OutputTest(unittest.TestCase):

    def test_lexer_error_handling(self):
        with io.StringIO() as output_buffer:
            with contextlib.redirect_stderr(output_buffer):
                lexer_error()
                self.assertEqual(output_buffer.getvalue(), "Error : Wrong number of arguments.\n")

    def test_parser_error_handling(self):
        with io.StringIO() as output_buffer:
            with contextlib.redirect_stderr(output_buffer):
                parser_error()
                self.assertEqual(output_buffer.getvalue(), "Error : Numerical characters only.\n")
