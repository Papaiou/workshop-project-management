import contextlib
import io
import unittest

from src.pushswap import pushswap


class IntegrationTest(unittest.TestCase):
    def test_integration(self):
        with io.StringIO() as output_buffer:
            with contextlib.redirect_stdout(output_buffer):
                pushswap([6, 84, 84, 426, 4526, 6376, 42864, 88266])
                self.assertEqual(output_buffer.getvalue(), "")
                pushswap([84, 84])
                self.assertEqual(output_buffer.getvalue(), "")
                pushswap([84])
                self.assertEqual(output_buffer.getvalue(), "")
                pushswap([4526, 6376, 84, 88266, 42864, 6, 426, 84])
                self.assertEqual(output_buffer.getvalue(),
                                 "pb, sa, pb, sb, pb, sa, pb, sa, pb, sb, sa, pb, "
                                 "sb, sa, pb, sb, pb, pa, pa, sb, pa, pa, sb, pa, "
                                 "sa, sb, pa, sa, sb, pa, pa, pb, pb, pb, pb, sa, "
                                 "pb, sb, pb, pb, pb, pa, pa, pa, pa, pa, pa, pa, pa")
            with contextlib.redirect_stderr(output_buffer):
                output_buffer.seek(0)
                output_buffer.truncate(0)
                pushswap(["a"])
                self.assertEqual(output_buffer.getvalue(), "Error : Numerical characters only.\n")

                output_buffer.seek(0)
                output_buffer.truncate(0)
                pushswap([])
                self.assertEqual(output_buffer.getvalue(), "Error : Wrong number of arguments.\n")
