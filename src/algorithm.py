from src.output_handler import output_writer


def is_a_list_sorted(a_list):
    sorted_list = a_list[:]
    sorted_list.sort()
    if a_list == sorted_list:
        return True
    return False


def list_swap(number_list):
    number_list[0], number_list[1] = number_list[1], number_list[0]
    return number_list


def a_list_handler(a_list, b_list):
    while len(a_list) != 0:
        if len(a_list) > 1 and a_list[0] > a_list[1]:
            a_list = list_swap(a_list)
            output_writer("sa")
        number_swapped = a_list.pop(0)
        b_list.insert(0, number_swapped)
        output_writer("pb")
        if len(b_list) > 1 and b_list[0] < b_list[1]:
            b_list = list_swap(b_list)
            output_writer("sb")
    return a_list, b_list


def b_list_handler(a_list, b_list):
    while len(b_list) != 0:
        if len(b_list) > 1 and b_list[0] < b_list[1]:
            b_list = list_swap(b_list)
            output_writer("sb")
        number_swapped = b_list.pop(0)
        a_list.insert(0, number_swapped)
        output_writer("pa")
        if len(a_list) > 1 and a_list[0] > a_list[1]:
            a_list = list_swap(a_list)
            output_writer("sa")
    return a_list, b_list


def monitor(numbers):
    a_list = numbers
    b_list = []
    while not is_a_list_sorted(a_list):
        a_list, b_list = a_list_handler(a_list, b_list)
        a_list, b_list = b_list_handler(a_list, b_list)
    return None
