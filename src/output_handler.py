is_first_move = True


def output_writer(move):
    global is_first_move
    if is_first_move:
        print(move, end='')
        is_first_move = False
    else:
        print(", " + move, end='')
