def parser(argument_list):
    number_list = []
    for argument in argument_list:
        try:
            number_list.append(int(argument))
        except ValueError:
            return None
    return number_list
