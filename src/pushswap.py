from src.lexer import lexer
from src.parser import parser
from src.algorithm import monitor
from src.error_handler import lexer_error, parser_error


def pushswap(arguments):
    if not lexer(arguments):
        return lexer_error()

    arguments = parser(arguments)
    if not arguments:
        return parser_error()

    return monitor(arguments)
