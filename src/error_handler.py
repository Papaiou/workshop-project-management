import sys


def lexer_error():
    print("Error : Wrong number of arguments.", file=sys.stderr)
    return None


def parser_error():
    print("Error : Numerical characters only.", file=sys.stderr)
    return None
