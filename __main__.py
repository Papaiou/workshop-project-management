import sys

from src.pushswap import pushswap

if __name__ == '__main__':
    arguments = sys.argv[1:]
    pushswap(arguments)
    exit(0)
